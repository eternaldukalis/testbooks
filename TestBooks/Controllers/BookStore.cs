using System;
using System.Collections.Generic;
using System.Linq;
using TestBooks.DataProviders;
using TestBooks.Elements;
using TestBooks.Output;

namespace TestBooks.Controllers
{
    /// <summary>
    /// Книжный магазин.
    /// </summary>
    public class BookStore
    {
        private const float CostMin = 200, CostMax = 1500;
        
        private readonly BaseBooksProvider _booksProvider;
        private Dictionary<BookStoreItem, Book> _items;
        
        /// <summary>
        /// Констуктор.
        /// </summary>
        /// <param name="booksProvider">Провайдер книг.</param>
        public BookStore(BaseBooksProvider booksProvider)
        {
            _booksProvider = booksProvider;
            booksProvider.LoadBooks();
            Book[] books = booksProvider.GetBooks();
            _items = new Dictionary<BookStoreItem, Book>();
            var random = new Random();
            for (var i = 0; i < books.Length; i++)
            {
                float cost = CostMin + (float)random.NextDouble() * (CostMax - CostMin);
                var item = new BookStoreItem(i, books[i].Info, cost);
                _items.Add(item, books[i]);
            }
        }

        /// <summary>
        /// Получить информацию обо всех книгах.
        /// </summary>
        /// <returns>Массив товаров.</returns>
        public BookStoreItem[] GetBooks()
        {
            return _items.Keys.ToArray();
        }

        /// <summary>
        /// Получить информацию о книгах данного автора.
        /// </summary>
        /// <param name="author">Автор книг.</param>
        /// <returns>Информация о книгах данного автора.</returns>
        public BookStoreItem[] GetBooksByAuthor(string author)
        {
            return _items.Keys.Where(x => x.BookInfo.Author == author).ToArray();
        }

        /// <summary>
        /// Получить информацию о книгах данного жанра.
        /// </summary>
        /// <param name="genre">Название жанра.</param>
        /// <returns>Информация о книгах данного жанра.</returns>
        public BookStoreItem[] GetBooksByGenre(string genre)
        {
            return _items.Keys.Where(x => x.BookInfo.Genre == genre).ToArray();
        }

        /// <summary>
        /// Купить книгу.
        /// </summary>
        /// <param name="item">Покупаемая книга.</param>
        /// <param name="bankAccount">Счёт, с которого производится покупка.</param>
        /// <returns>Купленная книга.</returns>
        public Book PurchaseBook(BookStoreItem item, BankAccount bankAccount)
        {
            var book = _items[item];
            var succeed = bankAccount.Withdraw(item.Cost);
            if (succeed)
                return book;
            AppConsole.LogError("Error while purchasing the book.");
            return null;
        }
    }
}