using TestBooks.Interfaces;

namespace TestBooks.Controllers
{
    /// <summary>
    /// Поисковик текста.
    /// </summary>
    public class TextSearcher
    {
        private readonly ITextSource _textSource;
        
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="textSource">Источник текста.</param>
        public TextSearcher(ITextSource textSource)
        {
            _textSource = textSource;
        }

        /// <summary>
        /// Найти строку в тексте.
        /// </summary>
        /// <param name="str">Искомая строка.</param>
        /// <returns>Позиция строки в тексте.</returns>
        public int Search(string str)
        {
            var text = _textSource.GetText();
            return text.IndexOf(str);
        }
    }
}