using System.Collections.Generic;
using System.Linq;
using TestBooks.Elements;
using TestBooks.Output;

namespace TestBooks.Controllers
{
    /// <summary>
    /// Банк.
    /// </summary>
    public class Bank
    {
        private const string NoAccountString = "This account doesn't exist.";
        
        private Dictionary<int, AccountInfo> _accounts;

        /// <summary>
        /// Констуктор.
        /// </summary>
        public Bank()
        {
            _accounts = new Dictionary<int, AccountInfo>();
        }

        /// <summary>
        /// Создать счёт.
        /// </summary>
        /// <returns>Созданный счёт.</returns>
        public BankAccount CreateAccount()
        {
            int id = _accounts.Count > 0 ? _accounts.Max(x => x.Key) + 1 : 0;
            var accountInfo = new AccountInfo(id);
            _accounts.Add(id, accountInfo);
            var bankAccount = new BankAccount(this, id);
            return bankAccount;
        }

        /// <summary>
        /// Снять деньги со счёта.
        /// </summary>
        /// <param name="id">Идентификатор счёта.</param>
        /// <param name="sum">Сумма для снятия.</param>
        /// <returns>Получилось ли снять деньни.</returns>
        public bool Withdraw(int id, float sum)
        {
            if (!_accounts.ContainsKey(id))
            {
                AppConsole.LogError(NoAccountString);
                return false;
            }

            return _accounts[id].Withdraw(sum);
        }

        /// <summary>
        /// Пополнить счёт.
        /// </summary>
        /// <param name="id">Идентификатор счёта.</param>
        /// <param name="sum">Сумма для пополнения.</param>
        public void Refill(int id, float sum)
        {
            if (!_accounts.ContainsKey(id))
            {
                AppConsole.LogError(NoAccountString);
                return;
            }
            
            _accounts[id].Refill(sum);
        }

        /// <summary>
        /// Получить баланс на счету.
        /// </summary>
        /// <param name="id">Идентификатор счёта.</param>
        /// <returns>Баланс.</returns>
        public float GetBalance(int id)
        {
            if (!_accounts.ContainsKey(id))
            {
                AppConsole.LogError(NoAccountString);
                return 0;
            }

            return _accounts[id].Balance;
        }
    }
}