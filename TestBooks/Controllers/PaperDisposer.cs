using TestBooks.Interfaces;
using TestBooks.Output;

namespace TestBooks.Controllers
{
    /// <summary>
    /// Установка для переработки бумаги.
    /// </summary>
    public class PaperDisposer
    {
        /// <summary>
        /// Избавитсья от бумаги.
        /// </summary>
        /// <param name="paper">Бумага.</param>
        public void DisposeOfPaper(IPaper paper)
        {
            paper.Dispose();
            AppConsole.Log("Disposed of some paper.");
        }
    }
}