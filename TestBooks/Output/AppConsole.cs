using System;

namespace TestBooks.Output
{
    /// <summary>
    /// Статический класс, используемый для вывода текста в консоль.
    /// </summary>
    public static class AppConsole
    {
        private const ConsoleColor ErrorColor = ConsoleColor.Red;
        
        /// <summary>
        /// Вывести текст в консоль.
        /// </summary>
        /// <param name="text">Выводимый текст.</param>
        public static void Log(string text)
        {
            Console.WriteLine(text);
        }

        /// <summary>
        /// Вывести текст ошибки в консоль.
        /// </summary>
        /// <param name="text">Текст ошибки.</param>
        public static void LogError(string text)
        {
            ConsoleColor previousColor = Console.ForegroundColor;
            Console.ForegroundColor = ErrorColor;
            Console.WriteLine(text);
            Console.ForegroundColor = previousColor;
        }
    }
}