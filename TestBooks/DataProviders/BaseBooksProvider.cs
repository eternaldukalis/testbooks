using System.Collections.Generic;
using TestBooks.Elements;

namespace TestBooks.DataProviders
{
    /// <summary>
    /// Базовый класс для провайдера книг.
    /// </summary>
    public abstract class BaseBooksProvider
    {
        protected List<Book> _books;
        
        /// <summary>
        /// Загрузить книги.
        /// </summary>
        public abstract void LoadBooks();

        /// <summary>
        /// Получить книги.
        /// </summary>
        /// <returns>Массив книг.</returns>
        public Book[] GetBooks()
        {
            return _books.ToArray();
        }
    }
}