using System.Collections.Generic;
using System.IO;
using TestBooks.Elements;
using TestBooks.Output;

namespace TestBooks.DataProviders
{
    /// <summary>
    /// Провайдер книг, полученных из текстового файла.
    /// </summary>
    public class TxtBooksProvider : BaseBooksProvider
    {
        private const int BookParametersCount = 4;
        
        private readonly string _path;
        
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="path">Путь к файлу.</param>
        public TxtBooksProvider(string path)
        {
            _path = path;
        }

        /// <summary>
        /// Загрузить книги.
        /// </summary>
        public override void LoadBooks()
        {
            _books = new List<Book>();
            var streamReader = new StreamReader(_path);
            while (!streamReader.EndOfStream)
            {
                string bookSource = streamReader.ReadLine();
                string[] parameters = bookSource.Split('\t');
                if (parameters.Length < BookParametersCount)
                {
                    AppConsole.LogError("Not enough parameters");
                    continue;
                }
                var bookInfo = new BookInfo(parameters[0], parameters[1], parameters[2]);
                var book = new Book(bookInfo, parameters[3]);
                _books.Add(book);
            }
        }
    }
}