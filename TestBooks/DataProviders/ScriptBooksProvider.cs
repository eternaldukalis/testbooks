using System.Collections.Generic;
using TestBooks.Elements;

namespace TestBooks.DataProviders
{
    /// <summary>
    /// Провайдер книг, полученных из скрипта.
    /// </summary>
    public class ScriptBooksProvider : BaseBooksProvider
    {
        /// <summary>
        /// Загрузить книги.
        /// </summary>
        public override void LoadBooks()
        {
            _books = new List<Book>();
            
            var bookInfo = new BookInfo("Book 1", "Author 1", "Genre 1");
            var book = new Book(bookInfo, "Bloody mary working ant");
            _books.Add(book);
            
            bookInfo = new BookInfo("Book 2", "Author 1", "Genre 1");
            book = new Book(bookInfo, "Life is a game stand alone");
            _books.Add(book);
            
            bookInfo = new BookInfo("Book 3", "Author 2", "Genre 2");
            book = new Book(bookInfo, "Crow song shine days");
            _books.Add(book);
            
            bookInfo = new BookInfo("Book 4", "Author 3", "Genre 3");
            book = new Book(bookInfo, "Manner mode everything catch me");
            _books.Add(book);
            
            bookInfo = new BookInfo("Book 5", "Author 4", "Genre 4");
            book = new Book(bookInfo, "Puzzle lullaby blue");
            _books.Add(book);
            
            bookInfo = new BookInfo("Book 6", "Author 4", "Genre 2");
            book = new Book(bookInfo, "Chaos logic hacking to the gate");
            _books.Add(book);
            
            bookInfo = new BookInfo("Book 7", "Author 5", "Genre 5");
            book = new Book(bookInfo, "Alert from the end");
            _books.Add(book);
            
            bookInfo = new BookInfo("Book 8", "Author 5", "Genre 5");
            book = new Book(bookInfo, "Gimme shelter brightness pajama kingdom crying");
            _books.Add(book);
            
            bookInfo = new BookInfo("Book 9", "Author 6", "Genre 5");
            book = new Book(bookInfo, "Days of dash choir jail");
            _books.Add(book);
        }
    }
}