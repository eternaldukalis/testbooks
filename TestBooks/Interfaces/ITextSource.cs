namespace TestBooks.Interfaces
{
    /// <summary>
    /// Интерфейс для источников текста.
    /// </summary>
    public interface ITextSource
    {
        /// <summary>
        /// Получить текст.
        /// </summary>
        /// <returns>Текст.</returns>
        string GetText();
    }
}