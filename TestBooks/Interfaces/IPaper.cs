namespace TestBooks.Interfaces
{
    /// <summary>
    /// Интерфейс для объектов, состоящих из бумаги.
    /// </summary>
    public interface IPaper
    {
        /// <summary>
        /// Избавиться от данного объекта.
        /// </summary>
        void Dispose();
    }
}