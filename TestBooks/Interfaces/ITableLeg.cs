namespace TestBooks.Interfaces
{
    /// <summary>
    /// Интерфейс для объектов, которые могут быть использованы в качестве ножки стола.
    /// </summary>
    public interface ITableLeg
    {
        /// <summary>
        /// Получить высоту ножки.
        /// </summary>
        /// <returns>Высота ножки.</returns>
        float GetLegHeight();
    }
}