namespace TestBooks.Elements
{
    /// <summary>
    /// Информация о банковском счёте.
    /// </summary>
    public class AccountInfo
    {
        /// <summary>
        /// Идентификатор счёта.
        /// </summary>
        public int Id { get; }
        /// <summary>
        /// Баланс на счету.
        /// </summary>
        public float Balance { private set; get; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="id">Идентификатор счёта.</param>
        public AccountInfo(int id)
        {
            Id = id;
        }

        
        /// <summary>
        /// Снять деньги со счёта.
        /// </summary>
        /// <param name="sum">Сумма для снятия.</param>
        /// <returns>Успешно ли сняты деньги.</returns>
        public bool Withdraw(float sum)
        {
            if (Balance < sum)
                return false;
            Balance -= sum;
            return true;
        }

        /// <summary>
        /// Пополнить счёт.
        /// </summary>
        /// <param name="sum">Сумма для пополнения.</param>
        public void Refill(float sum)
        {
            Balance += sum;
        }
    }
}