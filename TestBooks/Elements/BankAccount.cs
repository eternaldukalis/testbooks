using TestBooks.Controllers;

namespace TestBooks.Elements
{
    /// <summary>
    /// Счёт в банке.
    /// </summary>
    public class BankAccount
    {
        /// <summary>
        /// Баланс на счету.
        /// </summary>
        public float Balance => _issuer.GetBalance(_id);
        
        private Bank _issuer;
        private int _id;
        
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="issuer">Банк-эмитент.</param>
        /// <param name="id">Идентификатор счёта.</param>
        public BankAccount(Bank issuer, int id)
        {
            _issuer = issuer;
            _id = id;
        }

        /// <summary>
        /// Снять деньги со счёта.
        /// </summary>
        /// <param name="sum">Сумма для снятия.</param>
        /// <returns>Успешно ли сняты деньги.</returns>
        public bool Withdraw(float sum)
        {
            return _issuer.Withdraw(_id, sum);
        }

        /// <summary>
        /// Пополнить счёт.
        /// </summary>
        /// <param name="sum">Сумма для пополнения.</param>
        public void Refill(float sum)
        {
            _issuer.Refill(_id, sum);
        }
    }
}