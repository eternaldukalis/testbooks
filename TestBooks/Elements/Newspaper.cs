using System.Collections.Generic;
using System.Text;
using TestBooks.Interfaces;

namespace TestBooks.Elements
{
    /// <summary>
    /// Газета.
    /// </summary>
    public class Newspaper : ITextSource
    {
        /// <summary>
        /// Название газеты.
        /// </summary>
        public string Title { get; }

        private readonly List<string> _articles;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="title">Название.</param>
        /// <param name="articles">Статьи.</param>
        public Newspaper(string title, List<string> articles)
        {
            Title = title;
            _articles = articles;
        }

        /// <summary>
        /// Получить текст газеты.
        /// </summary>
        /// <returns>Текст газеты.</returns>
        public string GetText()
        {
            var strBuilder = new StringBuilder();
            
            foreach (var x in _articles)
            {
                strBuilder.Append(x);
            }

            return strBuilder.ToString();
        }
    }
}