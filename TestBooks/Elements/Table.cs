using TestBooks.Interfaces;

namespace TestBooks.Elements
{
    /// <summary>
    /// Стол.
    /// </summary>
    public class Table
    {
        /// <summary>
        /// Правая ножка.
        /// </summary>
        public ITableLeg RightLeg { set; get; }
        /// <summary>
        /// Левая ножка.
        /// </summary>
        public ITableLeg LeftLeg { set; get; }
    }
}