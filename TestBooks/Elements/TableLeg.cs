using TestBooks.Interfaces;

namespace TestBooks.Elements
{
    /// <summary>
    /// Ножка стола.
    /// </summary>
    public class TableLeg : ITableLeg
    {
        private readonly float _height;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="height">Высота ножки.</param>
        public TableLeg(float height)
        {
            _height = height;
        }

        /// <summary>
        /// Получить высоту ножки.
        /// </summary>
        /// <returns>Высота ножки.</returns>
        public float GetLegHeight()
        {
            return _height;
        }
    }
}