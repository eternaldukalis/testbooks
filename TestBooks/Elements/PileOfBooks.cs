using System.Linq;
using TestBooks.Interfaces;

namespace TestBooks.Elements
{
    /// <summary>
    /// Стопка книг.
    /// </summary>
    public class PileOfBooks : ITableLeg
    {
        private readonly Book[] _books;
        
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="books">Книги в стопке.</param>
        public PileOfBooks(params Book[] books)
        {
            _books = books;
        }

        /// <summary>
        /// Получить высоту стопки книг, подставленных под стол.
        /// </summary>
        /// <returns>Высота стопки.</returns>
        public float GetLegHeight()
        {
            return _books.Sum(x => x.Width);
        }
    }
}