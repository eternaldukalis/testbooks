namespace TestBooks.Elements
{
    /// <summary>
    /// Товар из книжного магазина.
    /// </summary>
    public class BookStoreItem
    {
        /// <summary>
        /// Идентификатор товара.
        /// </summary>
        public int Id { get; }
        /// <summary>
        /// Информация о книге.
        /// </summary>
        public BookInfo BookInfo { get; }
        /// <summary>
        /// Стоимость книги.
        /// </summary>
        public float Cost { get; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="id">Идентификатор.</param>
        /// <param name="bookInfo">Информация о книге.</param>
        /// <param name="cost">Стоимость.</param>
        public BookStoreItem(int id, BookInfo bookInfo, float cost)
        {
            Id = id;
            BookInfo = bookInfo;
            Cost = cost;
        }
    }
}