using TestBooks.Interfaces;

namespace TestBooks.Elements
{
    /// <summary>
    /// Книга.
    /// </summary>
    public class Book : ITextSource, IPaper
    {
        /// <summary>
        /// Базовая информация о книге.
        /// </summary>
        public BookInfo Info { private set; get; }

        /// <summary>
        /// Толщина книги.
        /// </summary>
        public float Width => _text.Length;

        private string _text;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="info">Информация о книге.</param>
        /// <param name="text">Текст книги.</param>
        public Book(BookInfo info, string text)
        {
            Info = info;
            _text = text;
        }

        /// <summary>
        /// Получить текст книги.
        /// </summary>
        /// <returns>Текст книги.</returns>
        public string GetText()
        {
            return _text;
        }

        /// <summary>
        /// Избавиться от книги.
        /// </summary>
        public void Dispose()
        {
            _text = null;
            Info = null;
        }
    }
}