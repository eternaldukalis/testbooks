namespace TestBooks.Elements
{
    /// <summary>
    /// Информация о книге.
    /// </summary>
    public class BookInfo
    {
        /// <summary>
        /// Название книги.
        /// </summary>
        public string Title { get; }
        /// <summary>
        /// Автор книги.
        /// </summary>
        public string Author { get; }
        /// <summary>
        /// Жанр книги.
        /// </summary>
        public string Genre { get; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="title">Название.</param>
        /// <param name="author">Автор.</param>
        /// <param name="genre">Жанр.</param>
        public BookInfo(string title, string author, string genre)
        {
            Title = title;
            Author = author;
            Genre = genre;
        }
    }
}