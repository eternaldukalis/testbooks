using TestBooks.Interfaces;

namespace TestBooks.Elements
{
    /// <summary>
    /// Бумажный мусор.
    /// </summary>
    public class PaperJunk : IPaper
    {
        /// <summary>
        /// Масса.
        /// </summary>
        public float Mass { private set; get; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="mass">Масса.</param>
        public PaperJunk(float mass)
        {
            Mass = mass;
        }

        /// <summary>
        /// Избавиться от мусора.
        /// </summary>
        public void Dispose()
        {
            Mass = 0;
        }
    }
}